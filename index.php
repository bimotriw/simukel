<?php 
include ("include/koneksi.php"); 
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<link rel="shortcut icon" href="images/simukel.png"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>SIMukel</title>
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <link rel="stylesheet" type="text/css" media="screen" href="css/reset.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="css/jquery.ketchup.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
<link href="css/front.css" media="screen, projection" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/templatemo_style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/arial.js"></script>
<script type="text/javascript" src="js/cuf_run.js"></script>
</head>
<body>
<div class="main">
  <div class="main_resize">
    <div class="header">
      <div class="logo">
        <h1><a href="#"><span>SI</span>MuKel<small><strong>Sistem Informasi Wali Murid &amp; Wali Kelas</strong></small></a><a href="#"></a></h1>
      </div>
      <div class="search">
        
          <!--<span>
          <input type="text" value="Search..." name="s" id="s" />
          <input name="search submit" type="image" src="images/search.gif" value="Go" id="searchsubmit" class="btn"  />
          </span>-->
        
        <!--/searchform -->
        <div class="clr"></div>
      </div>
      <div class="clr"></div>
      <div class="menu_nav">
        <ul class="navigation">
          <?php 
		   if($_GET[menu]=='home'){
			echo " 
				<li class='active'><a href='?menu=home'>Beranda<span class='ui_icon home'></span></a></li>
				<li><a href='?menu=contact'>Hubungi Kami<span class='ui_icon contact'></span></a></li>
				";}
				
			else if($_GET[menu]=='contact'){
			echo " 
				<li><a href='?menu=home'>Beranda<span class='ui_icon home'></span></a></li>
				<li class='active'><a href='?menu=contact'>Hubungi Kami<span class='ui_icon contact'></span></a></li>"
				;}else {
						echo "<script>document.location.href='?menu=home';</script>";
					}		?>
          
          </ul>
        
        
      </div>
       </div>
       
          <div id="container">
          
  
</div>

<script src="javascripts/jquery.tipsy.js" type="text/javascript"></script>
<script type='text/javascript'>
    $(function() {
	  $('#forgot_username_link').tipsy({gravity: 'w'});   
    });
  </script>
  
 <script type="text/javascript" src="js/jquery-1.4.js"></script>
    <script type="text/javascript" src="js/jquery.ketchup.js"></script>
    <script type="text/javascript" src="js/jquery.ketchup.messages.js"></script>
    <script type="text/javascript" src="js/jquery.ketchup.validations.basic.js"></script>

    <script type="text/javascript">
      $(document).ready(function() {
          $('#contoh').ketchup();
      });  
    </script> 
  
    
      <div class="clr"></div>
      
      
    
  <div class="fbg">
    <div class="fbg_resize">
      <div class="col c1">
       <div class="post">
          <div align="justify">
            <?php
                    if($_GET[menu]=="home")  {  
					echo " 
						</br>&emsp;&emsp;&emsp; <strong>Sistem Informasi Wali Murid dan Wali Kelas  (<em>SIMUKEL</em>)</strong> merupakan sistem informasi yang berfungsi sebagai penghubung antara wali kelas dan wali murid yang dapat memudahkan wali kelas selaku pihak sekolah untuk memberikan informasi mengenai nilai harian termasuk ujian tiap mata pelajaran serta daftar absensi murid kepada wali murid. Dengan adanya sistem ini, diharapkan wali murid dapat memantau perkembangan anaknya di sekolah. </br> &emsp;&emsp;&emsp; Sistem ini mempunyai hak akses kepada admin, guru, wali kelas, wali murid. Hak akses admin adalah untuk memasukkan data lengkap dari siswa, data guru mata pelajaran, data mata pelajaran, data daftar nilai setiap kelas dan rekap nilai buku induk siswa. Hak akses guru adalah untuk memasukkan nilai mata pelajaran siswa dalam satu kelas. Hak akses wali kelas hampir sama dengan guru namun wali kelas dapat melihat nilai semua mata pelajaran dalam satu kelas serta memberikan catatan untuk tiap murid yang ada dalam kelasnya dan dapat menginputkan absensi harian. Hak akses wali murid adalah untuk melihat rekap nilai tiap mata pelajaran anak mereka di sekolah  beserta absensi hariannya. </br> </br> <strong>Hasil Kerjasama</strong> :
						<div class='hbg'></div>
 <div class='col c4'>
 <img src='images/um.png' width='100' align='left'>
<strong> Universitas Negeri Malang<br /></strong> Fakultas Teknik <br /> Jurusan Teknik Elektro<br /> Pendidikan Teknik Informatika
</div>

<div class='col c5'>
 <img src='images/smp3logo.png' width='80' align='left'>
<strong>SMP Negeri 3 Malang<br /></strong> BINA TARUNA ADILOKA 
</div>

 </div> </br></br></br></br></br>
						";
					} else if ($_GET[menu]=="contact")  {  
					echo " 
					
						<h3><strong>Kontak Kami</strong></h3>
						<strong>SMP Negeri 3 Malang </strong>
						Jl. Dr. Cipto. nomer 20, Malang, Jawa Timur
						</br>Kode Pos: 65111
						</br>Telephone: (0341) 362612
						</br>Fax: (0341) 340224
						</br>Website: http://www.smpn3-mlg.sch.id
						</br>Anda juga dapat menghubungi kami melalui form di bawah ini :
            <div class='clr'></div>
           <div class='main_email'>
              <ol>
                <li>
                  <label for='name'>Nama</label>
                  <input id='name' name='name' class='text' />
                </li>
                <li>
                  <label for='email'>Alamat Email</label>
                  <input id='email' name='email' class='text' />
                </li>
                
                <li>
                  <label for='message'>Pesan Anda</label>
                  <textarea id='message' name='message' rows='8' cols='50'></textarea>
                </li>
                <li>
                  <div class='content_a_ad'>
								<input type='submit' value='Kirim'></input></div>
								<div class='clear'></div>
                </li>
              </ol>
          
          </div></div>
							
						";
					}else {
						echo "<script>document.location.href='?menu=home';</script>";
					}		 ?>
          </div>
                    
      </div></div>
                    
                    
                    
                    
      <div class="col c2">
        <h2><span><img src='images/icons/login.png' width="30" height="30" align='left'><strong>Masuk</strong></span></h2>
       <form action="include/cekloginwalmur.php" method="post" id="contoh">
        <div>
        </br>  
          <label for="nis"><strong>Nomer Induk Siswa / NIS</strong></label>
          <input type="text" name="nis" class="validate(required)" />
        </div>
        <div>
          <label for="password"><strong>Password</strong></label>
          <input type="password" name="pass" class="validate(required, minlength(5))" />
        </div>
      
      <div class="submit">
          <input type="submit" value="Proses" />
        </div>
        </br>
     </form>
    <?php
							$selectquery=mysql_query("select * from admin");
	  		$rowprof=mysql_fetch_array($selectquery);
			echo"
          <p>&nbsp;</p>
       <p><span><strong>Lupa Password Anda :</strong></span></br>
         Hubungi Admin </br>
				$rowprof[email] <img src='images/admin.png' width='100' align='right'></p>	
";
						?>
                        
       <script type="text/javascript"> 
    				// 1 detik = 1000 
   				 	window.setTimeout("waktu()",1000);   
    				function waktu() {    
       					var tanggal = new Date();   
        				setTimeout("waktu()",1000);   
       					document.getElementById("output").innerHTML = tanggal.getHours()+":"+tanggal.getMinutes()+":"+tanggal.getSeconds(); 
  					 } 
					
   				</script>
                
                <h2><font face=Digital-7><div id="output" align="center" text="black" onload="waktu()" > </div></h4>
             <h6><center><?php include('include/calendar.php');
				?></center>  </h6> 
              
              
                        
      </div>  
      <div class="clr"></div>
      
      
    </div>
    
  </div>
  
</div>


 
<div class="footer">
  <div class="footer_resize">
    <p class="lf">Universitas Negeri Malang --- Copyright © 2013</p>
    <p class="rf">SMP Negeri 3 Malang <a href="http://www.smpn3-mlg.sch.id /">Website</a></p>
    <div class="clr"></div>
  </div>
</div>
</body>
</html>