<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
  <link href="../../css/style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../../css/overlay-dialog.css">
    <script type="text/javascript" src="../../js/jquery-1.3.2.min.js"></script>
    <script type="text/javascript" src="../../js/script.js"></script>
    <script type="text/javascript" src="../../js/jquery.tools.min.js"></script>

    <script type="text/javascript"> 
      	$(document).ready(function(){
      	// expose form ketika form di klik atau kursor mouse berada di salah satu komponen form
			$("form.expose", function() {
          		$("form").expose({
			  		
			  		color: '#333',
		          	loadSpeed: 200,
		         	opacity: 0.9,
			   		api: true
				}).load();
	      	});
	    });
	  </script>	
  </head>
    <body>
    <!-- the triggers -->
    

    <!-- yes/no dialog -->
    <form class="expose">
    <div class="modal1" >
    <ul>
	     <h2>Peringatan !!</h2>
        <p>Maaf, NIS Sudah Dipakai. 
           Untuk menutup kotak dialog, klik tombol yes/no atau tekan ESC di keyboard.</p>

	     <!-- yes/no buttons -->
		<p class="fish"><a href= "../index.php?menu=vmurid" > Yes </a></p>
           </ul>
    </div>
    </form>
  </body>
</html>
