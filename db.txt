-- phpMyAdmin SQL Dump
-- version 3.1.3.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 09, 2014 at 07:55 PM
-- Server version: 5.1.33
-- PHP Version: 5.2.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `simukel`
--

-- --------------------------------------------------------

--
-- Table structure for table `absensi`
--

CREATE TABLE IF NOT EXISTS `absensi` (
  `id_absensi` int(3) NOT NULL AUTO_INCREMENT,
  `id_murid` int(3) NOT NULL,
  `keterangan` enum('hadir','sakit','izin','alpha') NOT NULL,
  `tanggal` varchar(10) NOT NULL,
  `id_tahun_ajar` int(11) NOT NULL,
  PRIMARY KEY (`id_absensi`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=276 ;

--
-- Dumping data for table `absensi`
--

INSERT INTO `absensi` (`id_absensi`, `id_murid`, `keterangan`, `tanggal`, `id_tahun_ajar`) VALUES
(212, 22, 'sakit', '14/03/2013', 1),
(211, 18, 'izin', '14/03/2013', 1),
(210, 27, 'alpha', '14/03/2013', 1),
(209, 5, 'izin', '14/03/2013', 1),
(208, 3, 'sakit', '14/03/2013', 1),
(207, 1, 'sakit', '14/03/2013', 1),
(213, 1, 'hadir', '15/03/2013', 1),
(214, 3, 'hadir', '15/03/2013', 1),
(215, 5, 'hadir', '15/03/2013', 1),
(216, 27, 'hadir', '15/03/2013', 1),
(217, 18, 'hadir', '15/03/2013', 1),
(218, 22, 'hadir', '15/03/2013', 1),
(219, 1, 'hadir', '16/03/2013', 1),
(220, 3, 'hadir', '16/03/2013', 1),
(221, 5, 'hadir', '16/03/2013', 1),
(222, 27, 'hadir', '16/03/2013', 1),
(223, 18, 'hadir', '16/03/2013', 1),
(224, 22, 'hadir', '16/03/2013', 1),
(225, 1, 'hadir', '17/03/2013', 1),
(226, 3, 'hadir', '17/03/2013', 1),
(227, 5, 'hadir', '17/03/2013', 1),
(228, 27, 'hadir', '17/03/2013', 1),
(229, 18, 'hadir', '17/03/2013', 1),
(230, 22, 'hadir', '17/03/2013', 1),
(231, 1, 'alpha', '18/03/2013', 1),
(232, 3, 'izin', '18/03/2013', 1),
(233, 5, 'sakit', '18/03/2013', 1),
(234, 27, 'hadir', '18/03/2013', 1),
(235, 18, 'hadir', '18/03/2013', 1),
(236, 22, 'sakit', '18/03/2013', 1),
(237, 1, 'izin', '11/04/2013', 1),
(238, 3, 'alpha', '11/04/2013', 1),
(239, 5, 'alpha', '11/04/2013', 1),
(240, 27, 'hadir', '11/04/2013', 1),
(241, 18, 'hadir', '11/04/2013', 1),
(242, 22, 'hadir', '11/04/2013', 1),
(243, 1, 'izin', '15/04/2013', 1),
(244, 3, 'sakit', '15/04/2013', 1),
(245, 5, 'izin', '15/04/2013', 1),
(246, 27, 'alpha', '15/04/2013', 1),
(247, 18, 'izin', '15/04/2013', 1),
(248, 22, 'hadir', '15/04/2013', 1),
(249, 1, 'izin', '17/04/2013', 1),
(250, 3, 'hadir', '17/04/2013', 1),
(251, 5, 'alpha', '17/04/2013', 1),
(252, 27, 'hadir', '17/04/2013', 1),
(253, 18, 'hadir', '17/04/2013', 1),
(254, 22, 'sakit', '17/04/2013', 1),
(255, 4, 'hadir', '16/04/2013', 1),
(256, 2, 'izin', '16/04/2013', 1),
(257, 6, 'alpha', '16/04/2013', 1),
(258, 1, 'hadir', '16/04/2013', 1),
(259, 9, 'hadir', '16/04/2013', 1),
(260, 1, 'hadir', '18/04/2013', 1),
(261, 3, 'hadir', '18/04/2013', 1),
(262, 5, 'hadir', '18/04/2013', 1),
(263, 27, 'hadir', '18/04/2013', 1),
(264, 18, 'hadir', '18/04/2013', 1),
(265, 22, 'alpha', '18/04/2013', 1),
(266, 4, 'izin', '18/04/2013', 1),
(267, 2, 'hadir', '18/04/2013', 1),
(268, 6, 'hadir', '18/04/2013', 1),
(269, 9, 'hadir', '18/04/2013', 1),
(270, 7, 'hadir', '18/04/2013', 1),
(271, 4, 'hadir', '19/04/2013', 1),
(272, 2, 'sakit', '19/04/2013', 1),
(273, 6, 'izin', '19/04/2013', 1),
(274, 7, 'alpha', '19/04/2013', 1),
(275, 9, 'hadir', '19/04/2013', 1);

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id_admin` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(36) NOT NULL,
  `email` varchar(200) NOT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `username`, `password`, `email`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'andhyardhianto@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `catatan`
--

CREATE TABLE IF NOT EXISTS `catatan` (
  `id_catatan` int(11) NOT NULL AUTO_INCREMENT,
  `id_murid` int(11) NOT NULL,
  `catatan` text NOT NULL,
  `status` varchar(4) NOT NULL,
  `tanggal` varchar(10) NOT NULL,
  `id_tahun_ajar` int(11) NOT NULL,
  PRIMARY KEY (`id_catatan`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `catatan`
--

INSERT INTO `catatan` (`id_catatan`, `id_murid`, `catatan`, `status`, `tanggal`, `id_tahun_ajar`) VALUES
(23, 4, 'Skripsi Yoo Brangkat', 'old', '18/04/2013', 1),
(24, 1, 'Tingkatkan belajarnya di rumah ', 'old', '21/04/2013', 1),
(19, 1, 'Bibiq Disana ', 'old', '16/04/2013', 1),
(20, 1, 'Hohoho Bibi Huhu', 'old', '17/04/2013', 1),
(22, 1, 'fareziz bkakaaaa', 'old', '18/04/2013', 1);

-- --------------------------------------------------------

--
-- Table structure for table `dtl_kelas`
--

CREATE TABLE IF NOT EXISTS `dtl_kelas` (
  `id_dtl_kelas` int(11) NOT NULL AUTO_INCREMENT,
  `id_dtl_wali_kelas` int(11) NOT NULL,
  `id_murid` int(11) NOT NULL,
  `id_tahun_ajar` int(11) NOT NULL,
  PRIMARY KEY (`id_dtl_kelas`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=127 ;

--
-- Dumping data for table `dtl_kelas`
--

INSERT INTO `dtl_kelas` (`id_dtl_kelas`, `id_dtl_wali_kelas`, `id_murid`, `id_tahun_ajar`) VALUES
(96, 1, 1, 1),
(97, 1, 3, 1),
(98, 2, 4, 1),
(99, 1, 5, 1),
(100, 2, 2, 1),
(101, 2, 6, 1),
(102, 2, 7, 1),
(103, 2, 9, 1),
(104, 1, 27, 1),
(105, 1, 18, 1),
(106, 1, 22, 1),
(107, 3, 30, 1),
(108, 3, 29, 1),
(109, 3, 28, 1),
(110, 3, 24, 1),
(111, 3, 26, 1),
(112, 3, 25, 1),
(113, 4, 23, 1),
(114, 4, 21, 1),
(115, 4, 19, 1),
(116, 4, 10, 1),
(117, 4, 13, 1),
(119, 4, 11, 1),
(120, 5, 8, 1),
(121, 5, 14, 1),
(122, 5, 17, 1),
(123, 6, 12, 1),
(124, 6, 15, 1),
(126, 5, 16, 1);

-- --------------------------------------------------------

--
-- Table structure for table `dtl_nilai`
--

CREATE TABLE IF NOT EXISTS `dtl_nilai` (
  `id_dtl_nilai` int(11) NOT NULL AUTO_INCREMENT,
  `id_murid` int(11) NOT NULL,
  `id_dtl_pengajaran` int(11) NOT NULL,
  `id_n_nilai` int(11) NOT NULL,
  `nilai` float NOT NULL,
  `id_tahun_ajar` int(11) NOT NULL,
  PRIMARY KEY (`id_dtl_nilai`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=612 ;

--
-- Dumping data for table `dtl_nilai`
--

INSERT INTO `dtl_nilai` (`id_dtl_nilai`, `id_murid`, `id_dtl_pengajaran`, `id_n_nilai`, `nilai`, `id_tahun_ajar`) VALUES
(605, 22, 62, 95, 80, 1),
(604, 18, 62, 95, 76, 1),
(603, 27, 62, 95, 86, 1),
(602, 5, 62, 95, 75, 1),
(601, 3, 62, 95, 80, 1),
(600, 1, 62, 95, 89, 1),
(599, 22, 62, 94, 85, 1),
(598, 18, 62, 94, 23, 1),
(597, 27, 62, 94, 90, 1),
(596, 5, 62, 94, 87, 1),
(595, 3, 62, 94, 89, 1),
(594, 1, 62, 94, 56, 1),
(593, 9, 66, 93, 86, 1),
(592, 7, 66, 93, 90, 1),
(591, 6, 66, 93, 87, 1),
(590, 2, 66, 93, 45, 1),
(589, 4, 66, 93, 75, 1),
(588, 9, 67, 92, 79, 1),
(587, 7, 67, 92, 80, 1),
(586, 6, 67, 92, 89, 1),
(585, 2, 67, 92, 90, 1),
(584, 4, 67, 92, 78, 1),
(583, 9, 67, 91, 67, 1),
(582, 7, 67, 91, 78, 1),
(581, 6, 67, 91, 90, 1),
(580, 2, 67, 91, 89, 1),
(579, 4, 67, 91, 67, 1),
(606, 1, 62, 96, 20.5, 1),
(607, 3, 62, 96, 67.5, 1),
(608, 5, 62, 96, 100, 1),
(609, 27, 62, 96, 90, 1),
(610, 18, 62, 96, 80, 1),
(611, 22, 62, 96, 70, 1);

-- --------------------------------------------------------

--
-- Table structure for table `dtl_pengajaran`
--

CREATE TABLE IF NOT EXISTS `dtl_pengajaran` (
  `id_dtl_pengajaran` int(11) NOT NULL AUTO_INCREMENT,
  `id_kelas` int(11) NOT NULL,
  `id_mata_pelajaran` int(11) NOT NULL,
  `id_guru` int(11) NOT NULL,
  `semester` enum('Ganjil','Genap') NOT NULL,
  `id_tahun_ajar` int(11) NOT NULL,
  PRIMARY KEY (`id_dtl_pengajaran`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=73 ;

--
-- Dumping data for table `dtl_pengajaran`
--

INSERT INTO `dtl_pengajaran` (`id_dtl_pengajaran`, `id_kelas`, `id_mata_pelajaran`, `id_guru`, `semester`, `id_tahun_ajar`) VALUES
(62, 1, 10, 1, 'Ganjil', 1),
(63, 1, 2, 1, 'Genap', 1),
(64, 1, 15, 5, 'Ganjil', 1),
(65, 1, 15, 5, 'Genap', 1),
(66, 2, 15, 5, 'Ganjil', 1),
(67, 2, 15, 5, 'Genap', 1),
(68, 2, 13, 6, 'Ganjil', 1),
(69, 1, 16, 8, 'Ganjil', 1),
(70, 4, 20, 5, 'Genap', 1),
(71, 6, 3, 2, 'Genap', 1),
(72, 15, 19, 6, 'Genap', 1);

-- --------------------------------------------------------

--
-- Table structure for table `dtl_wali_kelas`
--

CREATE TABLE IF NOT EXISTS `dtl_wali_kelas` (
  `id_dtl_wali_kelas` int(11) NOT NULL AUTO_INCREMENT,
  `id_kelas` int(11) NOT NULL,
  `id_guru` int(11) NOT NULL,
  `id_tahun_ajar` int(11) NOT NULL,
  PRIMARY KEY (`id_dtl_wali_kelas`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=694 ;

--
-- Dumping data for table `dtl_wali_kelas`
--

INSERT INTO `dtl_wali_kelas` (`id_dtl_wali_kelas`, `id_kelas`, `id_guru`, `id_tahun_ajar`) VALUES
(6, 6, 14, 1),
(5, 5, 2, 1),
(4, 4, 7, 1),
(3, 3, 6, 1),
(2, 2, 5, 1),
(1, 1, 1, 1),
(7, 7, 8, 1),
(8, 8, 9, 1),
(9, 9, 0, 1),
(10, 10, 0, 1),
(11, 11, 0, 1),
(12, 12, 0, 1),
(13, 13, 0, 1),
(14, 14, 0, 1),
(15, 15, 0, 1),
(16, 16, 0, 1),
(17, 17, 0, 1),
(18, 18, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE IF NOT EXISTS `guru` (
  `id_guru` int(10) NOT NULL AUTO_INCREMENT,
  `nip` varchar(20) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `password` varchar(36) NOT NULL,
  PRIMARY KEY (`id_guru`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `guru`
--

INSERT INTO `guru` (`id_guru`, `nip`, `nama`, `password`) VALUES
(4, '195610301977034', 'Siti Nur Layla ', '81513c4ba39b908a70b829ce2b9850f9'),
(3, '195610301977033', 'Bambang Pamungkas', '4f12fac7c7fea998b58808040cf4b9ae'),
(2, '195610301977032', 'Aurora Kumala', 'e8de153a34d1c445bc9748e48872d3b0'),
(1, '195610301977031', 'Andhy Ardhianto', '52a5ada1d340347222a4c7bf8ca39bac'),
(5, '195610301977035', 'Dedy Noviyanto ', '464175c727e1447d99aaf3b4cbe8037d'),
(6, '195610301977036', 'Erlina Kurnianingsih', 'de0144c4084669994e888ab69aad98d8'),
(7, '195610301977037', 'Nur Collis Dewanto ', 'a5b72ab59aedec0a3f67079cca28302e'),
(8, '195610301977038', 'Muhammad Rizal ', '69aa6255b09c40c5745e3ebd9c938bd5'),
(24, '12719393952', 'Siti', 'b4eacd342a7212d47a5f5bb151e43ac9');

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE IF NOT EXISTS `kelas` (
  `id_kelas` int(11) NOT NULL AUTO_INCREMENT,
  `kelas` varchar(11) NOT NULL,
  `tingkat` int(1) NOT NULL,
  PRIMARY KEY (`id_kelas`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `kelas`, `tingkat`) VALUES
(1, 'VII 1', 1),
(2, 'VII 2', 1),
(6, 'VII 6', 1),
(3, 'VII 3', 1),
(4, 'VII 4', 1),
(5, 'VII 5', 1),
(7, 'VII 7', 1),
(8, 'VII 8', 1),
(9, 'VII 9', 1),
(10, 'VII 10', 1),
(11, 'VIII 1', 2),
(12, 'VIII 2', 2),
(13, 'VIII 3', 2),
(14, 'VIII 4', 2),
(15, 'VIII 5', 2),
(16, 'VIII 6', 2),
(17, 'VIII 7', 2),
(18, 'VIII 8', 2),
(19, 'VIII 9', 2),
(20, 'VIII 10', 2),
(21, 'IX 1', 3),
(22, 'IX 2', 3),
(23, 'IX 3', 3),
(24, 'IX 4', 3),
(25, 'IX 5', 3),
(26, 'IX 6', 3),
(27, 'IX 7', 3),
(28, 'IX 8', 3),
(29, 'IX 9', 3),
(30, 'IX 10', 3);

-- --------------------------------------------------------

--
-- Table structure for table `mata_pelajaran`
--

CREATE TABLE IF NOT EXISTS `mata_pelajaran` (
  `id_mata_pelajaran` int(11) NOT NULL AUTO_INCREMENT,
  `mata_pelajaran` varchar(40) NOT NULL,
  `status` enum('Wajib','Mulok','Pilihan') NOT NULL,
  PRIMARY KEY (`id_mata_pelajaran`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `mata_pelajaran`
--

INSERT INTO `mata_pelajaran` (`id_mata_pelajaran`, `mata_pelajaran`, `status`) VALUES
(1, 'Bahasa Inggris ', 'Wajib'),
(2, 'Bahasa Indonesia ', 'Wajib'),
(3, 'Agama', 'Wajib'),
(4, 'PPKN', 'Wajib'),
(5, 'Teknologi Informasi dan Komunikasi', 'Wajib'),
(6, 'Matematika', 'Wajib'),
(7, 'Tata Busana', 'Wajib'),
(8, 'Pendidkan Jasmani dan Kesehatan', 'Wajib'),
(9, 'Fisika', 'Wajib'),
(10, 'Akutansi', 'Wajib'),
(11, 'Bimbingan Konseling', 'Wajib'),
(12, 'Sejarah', 'Wajib'),
(13, 'Biologi', 'Wajib'),
(14, 'Kimia', 'Wajib'),
(15, 'Elektro', 'Wajib'),
(16, 'Kesenian', 'Wajib'),
(17, 'Geografi', 'Wajib'),
(18, 'Ekonomi', 'Wajib'),
(19, 'Bahasa Daerah', 'Wajib'),
(20, 'Sejarah', 'Wajib'),
(21, 'Keterampilan', 'Pilihan'),
(22, 'TIK', 'Pilihan'),
(23, 'Tata Busana', 'Mulok'),
(24, 'Elektro', 'Mulok'),
(25, 'Bimbingan Konseling', 'Mulok'),
(26, 'Bahasa Daerah', 'Mulok');

-- --------------------------------------------------------

--
-- Table structure for table `murid`
--

CREATE TABLE IF NOT EXISTS `murid` (
  `id_murid` int(11) NOT NULL AUTO_INCREMENT,
  `nis` varchar(10) NOT NULL,
  `foto` text NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis_kelamin` varchar(50) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `kode_pos` varchar(50) NOT NULL,
  `agama` varchar(50) NOT NULL,
  `hobi` varchar(50) NOT NULL,
  `cita_cita` varchar(50) NOT NULL,
  `jarak_rumah` varchar(50) NOT NULL,
  `trans_sekolah` varchar(50) NOT NULL,
  `jml_saudara` int(50) NOT NULL,
  `nama_ayah` varchar(50) NOT NULL,
  `nama_ibu` varchar(50) NOT NULL,
  `kerja_ayah` varchar(20) NOT NULL,
  `kerja_ibu` varchar(20) NOT NULL,
  `pendidikan_ayah` varchar(50) NOT NULL,
  `pendidikan_ibu` varchar(50) NOT NULL,
  `penghasilan_ayah` varchar(50) NOT NULL,
  `penghasilan_ibu` varchar(50) NOT NULL,
  `no_UASBN` varchar(20) NOT NULL,
  `no_SKHUASBN` varchar(20) NOT NULL,
  `asal_sekolah` varchar(50) NOT NULL,
  `status_sekolah` varchar(20) NOT NULL,
  `jenis_sekolah` varchar(20) NOT NULL,
  `kab_kota` varchar(20) NOT NULL,
  `tahun_masuk` int(20) NOT NULL,
  `status` enum('Aktif','Alumni') NOT NULL,
  `password` varchar(36) NOT NULL,
  PRIMARY KEY (`id_murid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=68 ;

--
-- Dumping data for table `murid`
--

INSERT INTO `murid` (`id_murid`, `nis`, `foto`, `nama_lengkap`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin`, `alamat`, `no_telp`, `kode_pos`, `agama`, `hobi`, `cita_cita`, `jarak_rumah`, `trans_sekolah`, `jml_saudara`, `nama_ayah`, `nama_ibu`, `kerja_ayah`, `kerja_ibu`, `pendidikan_ayah`, `pendidikan_ibu`, `penghasilan_ayah`, `penghasilan_ibu`, `no_UASBN`, `no_SKHUASBN`, `asal_sekolah`, `status_sekolah`, `jenis_sekolah`, `kab_kota`, `tahun_masuk`, `status`, `password`) VALUES
(30, '9978455930', '', 'Dwiki Wahyu Primata', 'Malang', '2002-04-16', 'Laki-laki', 'Malang', '', '5345', 'Islam', 'Kesenian', 'Seniman', '3-5km', 'Mobil Pribadi', 2, 'Tomas Farhat', 'Rerita Tralala', 'Dokter', 'Pegawai swasta', 'S2', 'S1', 'Lebih dari Rp. 5 juta', '', '543534', '34634', 'SDN Sawojajar 1', 'Negeri', 'SD', 'Malang', 1, 'Aktif', 'bb6c309d21b3139fda5aec1224ff3ee0'),
(29, '9978455929', '', 'Putri Ardhita', 'Malang', '2002-04-18', '', 'Malang', '', '234235', 'Islam', 'Olahraga', 'Politikus', '5-10km', 'Motor', 2, 'Rohman Fauzi', 'Fafa Filacea', 'Dokter', 'Seniman', 'S2', 'S2', 'Lebih dari Rp. 5 juta', '', '56456', '4654', 'SDN Percobaan 1', 'Negeri', 'SD', 'Malang', 1, 'Aktif', '5734c92fe8462385cc00b8f1aa128f95'),
(28, '9978455928', '', 'Wangi Kaoru', 'Malang', '2001-04-10', 'Perempuan', 'Malang', '', '4564', 'Islam', 'Menulis', 'Lainnya', '5-10km', 'Mobil Pribadi', 2, 'Pohan Simatupang', 'Bianca Walqurani', 'Guru/Dosen', 'Guru/Dosen', 'S2', 'S2', 'Rp. 3 juta s/d Rp. 5 juta', '', '4363', '563', 'SDN Kauman 1', 'Negeri', 'SD', 'Malang', 1, 'Aktif', '0bdd8494da3db551b3b2593c686ae091'),
(27, '9978455927', '', 'Tomy Candra ', 'Malang', '2002-01-12', 'Laki-laki', 'Malang', '', '645645', 'Islam', 'Olahraga', 'PNS', '1-3km', 'Motor', 2, 'Ronny Sianturi', 'Rosma Rosalita', 'Dokter', 'PNS', 'S2', 'S1', 'Lebih dari Rp. 5 juta', '', '5343', '3643', 'SDN Kauman ', 'Negeri', 'SD', 'Malang', 1, 'Aktif', ''),
(26, '9978455926', '', 'Witri Handayani', 'Tulungagung', '2000-05-06', 'Perempuan', 'Malang', '', '657567', 'Islam', 'Kesenian', 'POLRI', '3-5km', 'Antar Jemput Sekolah', 2, 'Wawan Ismawan', 'Leyla', 'Buruh', 'Pedagang/Wiraswasta', 'S1', 'S1', 'Rp. 3 juta s/d Rp. 5 juta', '', '5647', '547547', 'SDN Tambang Rejo', 'Negeri', 'SD', 'Malang', 1, 'Aktif', '71eabb7c608cd76f7732dc8239e8c6d8'),
(25, '9978455925', '', 'Nunung Nitinegoro', 'Jombang', '2001-08-06', 'Perempuan', 'Polehan ', '', '54654', 'Islam', 'Travelling', 'Guru/Dosen', '3-5km', 'Mobil Pribadi', 5, 'Jarwo Kuat', 'Fitriani ', 'PNS', 'PNS', 'S2', 'S2', 'Rp. 3 juta s/d Rp. 5 juta', '', '4654', '56456', 'SDN Tambang Rejo', 'Negeri', 'SD', 'Malang', 1, 'Aktif', ''),
(23, '9978455923', '', 'Nor Risky Saoutra', 'Mojokerto', '1999-07-05', 'Laki-laki', 'Malang', '', '56756', 'Islam', 'Menulis', 'POLRI', '5-10km', 'Mobil Pribadi', 3, 'Wahyu Sakti ', 'Yulli Yuniarti', 'Guru/Dosen', 'Guru/Dosen', 'S3', 'S2', 'Lebih dari Rp. 5 juta', '', '56756', '57567', 'Min', 'Negeri', 'MI', 'Malang', 1, 'Aktif', ''),
(24, '9978455924', '', 'Aulia Kurnia Sari', 'Malang', '2000-08-06', 'Perempuan', 'Malang', '', '546546', 'Islam', 'Kesenian', 'Dokter', '3-5km', 'Mobil Pribadi', 4, 'Dedi Kombuser', 'Shela Rahma', 'Pedagang/Wiraswasta', 'Pegawai swasta', 'S2', 'S1', 'Rp. 1 juta s/d Rp. 3 juta', '', '65756', '56757', 'SDN Bunul Rejo', 'Negeri', 'SD', 'Malang', 1, 'Aktif', '52209dc9cfe9d52b787c4d5639fd1a34'),
(22, '9978455922', '../foto/latifah.JPG', 'Alifi Shahrofi ', 'Tuban ', '2000-06-05', 'Perempuan', 'Malang', '', '56456', 'Islam', 'Kesenian', 'Guru/Dosen', '3-5km', 'Antar Jemput Sekolah', 4, 'Mahmud Dinejat', 'Roiza Silalahi', 'Pedagang/Wiraswasta', 'Di rumah saja', 'S2', 'S1', 'Rp. 3 juta s/d Rp. 5 juta', '', '64564', '47474', 'SDN Istiklal', 'Negeri', 'MI', 'Malang', 1, 'Aktif', '3e18c0248e725c6015f05d5633f1032d'),
(21, '9978455921', '', 'Wetinda Kusumawardani ', 'Batu', '2000-07-06', 'Perempuan', 'Malang ', '', '567568', 'Budha', 'Kesenian', 'POLRI', '5-10km', 'Mobil Pribadi', 1, 'Alehandro Alfonso', 'Faresttin ', 'Pegawai swasta', 'Pedagang/Wiraswasta', 'S2', '', 'Rp. 3 juta s/d Rp. 5 juta', '', '567567', '756756', 'SDN MEkar Sari', 'Swasta', 'MI', 'Malang', 1, 'Aktif', ''),
(20, '9978455920', '', 'Salahudin ', 'Malang ', '2000-09-18', 'Laki-laki', 'Malang ', '', '57567', 'Islam', 'Kesenian', 'Politikus', '3-5km', 'Angkutan Umum', 3, 'Dino Cahyo Aditya ', 'Wirra Wirri ', 'Dokter', 'Di rumah saja', 'S1', 'S2', 'Rp. 3 juta s/d Rp. 5 juta', '', '547644', '46746746', 'SDN Percobaan ', 'Negeri', 'SD', 'Malang ', 1, 'Aktif', ''),
(19, '9978455919', '', 'Ninna Ayu Binadari', 'Malang', '2000-07-08', 'Perempuan', 'Lamongan ', '', '474674', 'Katholik', 'Menulis', 'Dokter', '5-10km', 'Antar Jemput Sekolah', 4, 'Hanum Bramantyo', 'Enjel Elga', 'Pegawai swasta', 'Di rumah saja', 'S1', 'S1', 'Rp. 1 juta s/d Rp. 3 juta', '', '346345', '356', 'MIN', 'Negeri', 'MI', 'Malang', 1, 'Aktif', ''),
(18, '9978455918', '', 'Gumelar Danang Aryo', 'Melbourne', '2000-07-16', 'Perempuan', 'Malang ', '', '467565', 'Islam', 'Membaca', 'Guru/Dosen', '3-5km', 'Motor', 2, 'Agus Sakti', 'Rerita Tralala', 'Pegawai swasta', 'Pegawai swasta', 'S1', 'S2', 'Rp. 3 juta s/d Rp. 5 juta', '', '436546', '54754', 'Global ', 'Swasta', 'MI', 'Jakarta', 1, 'Aktif', ''),
(17, '9978455917', '', 'Cludia Cinta Bella ', 'Tulungagung ', '2001-02-28', 'Perempuan', 'PBI ', '', '35636', 'Islam', 'Membaca', 'Seniman', '3-5km', 'Mobil Pribadi', 3, 'Bisma Agung ', 'Sherly Oktavia', 'Pedagang/Wiraswasta', 'Pegawai swasta', 'S2', 'S1', 'Rp. 3 juta s/d Rp. 5 juta', '', '4645', '765756', 'SDN Bunul Rejoi', 'Negeri', 'SD', 'Malang', 1, 'Aktif', ''),
(16, '9978455916', '', 'Alexandro Delpiero ', 'Malang ', '2001-01-15', 'Laki-laki', 'Araya Bumi Perkasa ', '', '3523', 'Katholik', 'Travelling', 'Dokter', '5-10km', 'Motor', 2, 'Putra Mahkota ', 'Annisa Dwi Cahyaningsih', 'Dokter', 'Dokter', 'S1', 'S2', 'Lebih dari Rp. 5 juta', '', '67567', '865856', 'SDN P ', 'Negeri', 'SD', 'Malang ', 1, 'Aktif', ''),
(14, '9978455914', '../foto/Custom-Icon-Design-Pretty-Office-10-Teacher-female.ico', 'Alexandria Geraldii', 'Malang ', '2001-04-17', 'Perempuan', 'Ksatria Agung ', '', '654767', 'Katholik', 'Kesenian', 'POLRI', '1-3km', 'Mobil Pribadi', 3, 'Maicail Anjeloo ', 'Srilangka ', 'Politikus', 'Pedagang/Wiraswasta', 'S2', 'D3/D4', 'Lebih dari Rp. 5 juta', '', '56', '4564', 'MIN ', 'Negeri', 'SD', 'Malang ', 1, 'Aktif', ''),
(15, '9978455915', '', 'Arie Wahyu Susilo ', 'Kediri ', '2004-03-14', 'Laki-laki', 'Malang Suukun', '', '65654', 'Islam', 'Kesenian', 'Lainnya', '1-3km', 'Antar Jemput Sekolah', 3, 'Parjo Supeno ', 'Sukaimin Azizah ', 'Pedagang/Wiraswasta', 'Pedagang/Wiraswasta', 'SMA/MA/Paket C', '', 'Rp. 3 juta s/d Rp. 5 juta', '', '34534534', '4563563', 'SDN Percobaan ', 'Negeri', 'SD', 'Malang ', 1, 'Aktif', ''),
(11, '9978455911', '../foto/latifah.JPG', 'Gita Fausiah ', 'Malang', '2000-02-18', 'Perempuan', 'Malang', '(0341) 713857', '6576', 'Islam', 'Olahraga', 'POLRI', '1-3km', 'Motor', 2, 'Andi Lesmana', 'Yayang Alvin ', 'PNS', 'Dokter', 'D1/D2', 'SMA/MA/Paket C', 'Rp. 3 juta s/d Rp. 5 juta', '', '5346', '24542', 'Deae', 'Swasta', 'SD', 'Malang', 1, 'Aktif', 'c7abebc5578f8cf22f78ac050cddb98a'),
(12, '9978455912', '', 'Burhan Simanjuntak', 'Batu', '2000-01-15', 'Laki-laki', 'Malang', '', '876867', 'Katholik', 'Olahraga', 'Guru/Dosen', '3-5km', 'Mobil Pribadi', 3, 'Sony Silalalhi', 'Siti Maimunah', 'PNS', 'Buruh', 'S1', 'S2', 'Rp. 3 juta s/d Rp. 5 juta', '', '5656', '658568', 'MIN ', 'Negeri', 'MI', 'Malang', 1, 'Aktif', 'cfcb2e77b164a6b28ee9eeba17cfc9e7'),
(13, '9978455913', '../foto/Graduate-female.png', 'Herlambang Pamungkas', 'Malang', '2003-02-18', 'Laki-laki', 'Malang', '', '4656', 'Katholik', 'Travelling', 'Politikus', '1-3km', 'Sepeda', 2, 'Toni Marcelino ', 'Devi Eka', 'Seniman', 'Di rumah saja', 'S1', 'S1', 'Rp. 3 juta s/d Rp. 5 juta', '', '67867', '867967', 'MIN ', 'Negeri', 'MI', 'Malang', 1, 'Aktif', '822b6eb18412728dadd1b95d5029319b'),
(10, '9978455910', '292266_4493440863763_83882443_n.jpg', 'Samsul Bahri', 'Tulungagung', '2002-05-18', 'Laki-laki', 'Malang ', '', '65756', '', 'Kesenian', 'Guru/Dosen', '5-10km', 'Antar Jemput Sekolah', 3, 'Paijo ', 'Tukiyem ', 'Buruh', 'Petani/Peternak', 'D3/D4', 'D3/D4', 'Rp. 3 juta s/d Rp. 5 juta', '', '6456', '7', 'MIN', 'Negeri', 'SD', 'Kab Malang', 1, 'Aktif', '5dc0d4f92cd354cf60c1f6057468f0ca'),
(9, '9978455999', '', 'Widi Aldiano', 'Malang', '2000-05-06', 'Laki-laki', 'Malang', '', '65875', '', 'Kesenian', 'POLRI', '5-10km', 'Motor', 2, 'Sulasnoko', '78', 'Pegawai swasta', 'Di rumah saja', 'D3/D4', 'S1', 'Rp. 3 juta s/d Rp. 5 juta', '', '3636', '4757', 'MIN', 'Swasta', 'MI', 'dsfds', 1, 'Aktif', '2a601f48096592f357198a334e3a093f'),
(8, '9978455998', '', 'Anam Windiatmaka', 'Banyuwangi', '2001-09-13', 'Laki-laki', 'Mboreng Satelit', '', '89879', 'Islam', 'Kesenian', 'Guru/Dosen', '3-5km', 'Motor', 3, 'Faris Azrofi', 'Ninik Kurniasari', 'Pedagang/Wiraswasta', 'Pedagang/Wiraswasta', 'S2', 'S1', 'Lebih dari Rp. 5 juta', '', '65857', '7689679', 'SDN Mboreng 4', 'Negeri', 'SD', 'Malang', 1, 'Aktif', '1cec96335ac98e90a3a10c015946ca3c'),
(7, '9978455997', 'cantik.jpg', 'Samsul Hadi ', 'Malang ', '2001-03-15', 'Laki-laki', 'Grebek ', '', '3425', 'Islam', 'Membaca', 'POLRI', '3-5km', 'Mobil Pribadi', 3, 'Warsono ', 'Suprihatin ', 'Petani/Peternak', 'Pedagang/Wiraswasta', 'SMA/MA/Paket C', 'SMA/MA/Paket C', 'Rp. 1 juta s/d Rp. 3 juta', '', '47467', '47467', 'SDN Ngurah Reejo ', 'Negeri', 'SD', 'Kab. Malang', 1, 'Aktif', '42efff7de86a242a284948f90a76751c'),
(6, '9978455996', '', 'Tomy Subagio Darmono', 'Nganjuk ', '2001-10-05', 'Laki-laki', 'Landungsari ', '', '7699', 'Islam', 'Travelling', 'Politikus', '5-10km', 'Motor', 2, 'Gatot Brahmana ', 'Sulistiowati ', 'PNS', 'Di rumah saja', 'S1', 'S1', 'Rp. 3 juta s/d Rp. 5 juta', '', '67468', '48864', 'MIN ', 'Negeri', 'MI', 'Malang', 1, 'Aktif', '1cbe80bb5da343a738cbd00cfefb8466'),
(5, '9978455995', '', 'Imade Kethut Aksara', 'Kuta Bali ', '2001-09-18', 'Laki-laki', 'Kalpataru Malang', '', '58759', 'Hindu', 'Kesenian', 'Lainnya', '3-5km', 'Angkutan Umum', 2, 'Puthu Bagus ', 'Ayu Binadari ', 'Pegawai swasta', 'Pegawai swasta', 'S1', 'S1', 'Rp. 3 juta s/d Rp. 5 juta', '', '467', '8648', 'SDN 1 Lowokwaru ', 'Negeri', 'SD', 'Malang', 1, 'Aktif', 'cc8568e6defdfa6a4ec10839d308c66c'),
(4, '9978455994', '', 'Dela Contetta Marcelina', 'Canada ', '2000-04-12', 'Perempuan', 'Puri Cempaka Tidar ', '', '9090', 'Protestan', 'Olahraga', 'Lainnya', '5-10km', 'Antar Jemput Sekolah', 3, 'Brotoceno Santoso', 'Berta Lamusu', 'Pedagang/Wiraswasta', 'Pedagang/Wiraswasta', 'S1', 'S1', 'Lebih dari Rp. 5 juta', '', '678658', '796975', 'SD Klose Santo Yusuf ', 'Swasta', 'SD', 'Malang', 1, 'Aktif', '8f2d5c96231c77a3c10da100ce4674c9'),
(3, '9978455993', '', 'Antonius Coponlusi', 'Jakarta', '2002-02-18', 'Laki-laki', 'Simpang Sulfat Utara', '', '67898', 'Katholik', 'Travelling', 'Seniman', '1-3km', 'Motor', 4, 'Bambang Bramantyo', 'Monica Anjelik ', 'Dokter', 'Dokter', 'S1', 'S1', 'Lebih dari Rp. 5 juta', '', '5674', '73735', 'SDN Global Warming', 'Negeri', 'SD', 'Jakarta Selatan', 1, 'Aktif', '70418d359ca9fc474b0abcc7354d54ea'),
(2, '9978455992', '', 'Ali Ahmad Santoso', 'Saudi Arabia', '2000-11-10', 'Laki-laki', 'Sukun ', '', '7890', 'Islam', 'Kesenian', 'Guru/Dosen', '5-10km', 'Angkutan Umum', 5, 'Darmono Ali', 'Siti Khatijdah', 'Pedagang/Wiraswasta', 'Pedagang/Wiraswasta', 'S2', 'S2', 'Rp. 3 juta s/d Rp. 5 juta', '', '6768', '8709', 'SDN Percobaan', 'Negeri', 'SD', 'Malang', 1, 'Aktif', '0de23207e49b5cc8fb92379c4abbfb0e'),
(1, '9978455991', 'cantik.jpg', 'Marfuah Sri Astuti', 'Kepanjen', '2000-02-03', 'Perempuan', 'Sawojajar Malang', '', '7890', 'Islam', 'Membaca', 'Dokter', '1-3km', 'Mobil Pribadi', 0, 'Ahmad Zainudin Galih', 'Tata Dwi Ariska', 'PNS', 'PNS', 'S2', 'S2', 'Rp. 3 juta s/d Rp. 5 juta', '', '567657', '9890890', 'MIN', 'Negeri', 'SD', 'Malang', 1, 'Aktif', '1ca7fe10c72007ca10a927d6dcc1494a');

-- --------------------------------------------------------

--
-- Table structure for table `n_nilai`
--

CREATE TABLE IF NOT EXISTS `n_nilai` (
  `id_n_nilai` int(11) NOT NULL AUTO_INCREMENT,
  `nama_nilai` varchar(15) NOT NULL,
  `id_dtl_pengajaran` int(11) NOT NULL,
  PRIMARY KEY (`id_n_nilai`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=97 ;

--
-- Dumping data for table `n_nilai`
--

INSERT INTO `n_nilai` (`id_n_nilai`, `nama_nilai`, `id_dtl_pengajaran`) VALUES
(96, 'T 3', 62),
(95, 'T 2', 62),
(94, 'T 1', 62),
(93, 'T 6', 66),
(92, 'T 2', 67),
(91, 'T 1', 67);

-- --------------------------------------------------------

--
-- Table structure for table `tahun_ajar`
--

CREATE TABLE IF NOT EXISTS `tahun_ajar` (
  `id_tahun_ajar` int(11) NOT NULL AUTO_INCREMENT,
  `tahun_ajar_a` int(11) NOT NULL,
  `tahun_ajar_b` int(11) NOT NULL,
  PRIMARY KEY (`id_tahun_ajar`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tahun_ajar`
--

INSERT INTO `tahun_ajar` (`id_tahun_ajar`, `tahun_ajar_a`, `tahun_ajar_b`) VALUES
(1, 2013, 2014);
