<?php
// include class file
include 'Spreadsheet/Excel/Writer.php';
include '../../include/koneksi.php';

// initialize reader object
$excel = new Spreadsheet_Excel_Writer();


// attempt a connection
try {
   $pdo = new PDO('mysql:dbname=simukel;host=localhost', 'root', '');
} catch (PDOException $e) {
   die("ERROR: Could not connect: " . $e->getMessage());
}

$id = $_GET[id];
$id_mp = $_GET[id_mp];

// send client headers

$sql = "SELECT * FROM dtl_nilai";

 $q_th = mysql_query("select * from tahun_ajar order by id_tahun_ajar desc");
  $r_th = mysql_fetch_array($q_th);
  $q_g = mysql_query("SELECT * from guru WHERE nip='$id'");
  $r_g = mysql_fetch_array($q_g);
  $query_p = mysql_query("SELECT * FROM dtl_pengajaran WHERE id_dtl_pengajaran = '$id_mp' AND id_tahun_ajar='$r_th[id_tahun_ajar]' AND id_guru='$r_g[id_guru]'");
  $row_p = mysql_fetch_array($query_p);
  $query_mp = mysql_query("SELECT * FROM mata_pelajaran WHERE id_mata_pelajaran = '$row_p[id_mata_pelajaran]'");
  $row_mp = mysql_fetch_array($query_mp);


$excel->send(''.$r_g[nama].', '.$row_mp[mata_pelajaran].', Semester '.$row_p[semester].'.xls');

// add worksheet
$sheet =& $excel->addWorksheet('Sheet 1');
$sheet->setColumn(1,1,17.86);
$sheet->setColumn(2,2,28);
$sheet->insertBitmap(0 ,0 ,"smp3logo.bmp" ,60 ,4 ,0.7 ,0.6);


// read data from database
// convert into spreadsheet
$rowCount = 0;

$jud =& $excel->addFormat();
$jud->setFontFamily('Times New Roman');
$jud->setSize(14);
$jud->setBold();

$bld =& $excel->addFormat();
$bld->setFontFamily('Times New Roman');
$bld->setSize(11);
$bld->setBold();

$men =& $excel->addFormat();
$men->setFontFamily('Times New Roman');
$men->setSize(11);
$men->setBold();

$hdr =& $excel->addFormat();
$hdr->setFontFamily('Times New Roman');
$hdr->setSize(11);
$hdr->setBold();
$hdr->setBottom(2);
$hdr->setTop(2);
$hdr->setRight(2);
$hdr->setLeft(2);
$hdr->setAlign('Center');

$we =& $excel->addFormat();
$we->setFontFamily('Times New Roman');
$we->setSize(11);
$we->setAlign('Left');



if ($result = $pdo->query($sql)) {
  // get header row   
  $sheet->write(2, 2, "SMP NEGERI 3 MALANG", $jud);
  $sheet->write(5, 1, "Mata Pelajaran   :", $bld);
  $sheet->write(6, 1, "Semester            :", $bld);
  $sheet->write(7, 1, "Guru Pengajar    :", $bld);
  $sheet->write(8, 1, "Nama Nilai          :", $bld); 
  
 
  
  $sheet->write(5, 2, $row_mp[mata_pelajaran], $men);
  $sheet->write(6, 2, $row_p[semester], $men);
  $sheet->write(7, 2, $r_g[nama], $men);     
  
  $sheet->write(10, 1, "NIS", $hdr);
  $sheet->write(10, 2, "Nama", $hdr);
  $sheet->write(10, 3, "Nilai", $hdr);
  // get data rows
  
  $query_k = mysql_query("SELECT * FROM dtl_wali_kelas WHERE id_kelas = '$row_p[id_kelas]' AND id_tahun_ajar='$r_th[id_tahun_ajar]'");
  $row_k = mysql_fetch_array($query_k);
  $query_n = mysql_query("SELECT DISTINCT id_n_nilai FROM dtl_nilai WHERE id_dtl_pengajaran = '$row_p[id_dtl_pengajaran]' ORDER BY id_n_nilai");

$result = mysql_query("SELECT * FROM dtl_kelas WHERE id_dtl_wali_kelas = '$row_k[id_dtl_wali_kelas]'");
$y = 0;
while($row = mysql_fetch_array($result)) {
	$a[$y] = $row['id_murid'];
	$y++;
}
$e=11;
for ($i = 0; $i < $y ; $i++) {
	$query_nm1 = mysql_query("SELECT * FROM dtl_nilai WHERE id_murid = '$a[$i]' AND id_n_nilai = '$b[0]'");
	$row_nm1 = mysql_fetch_array($query_nm1);
	$query_s = mysql_query("SELECT * FROM murid WHERE id_murid = '$a[$i]'");
	$row_s = mysql_fetch_array($query_s);
	$sheet->write($e, 1, $row_s[nis], $we);
	$sheet->write($e, 2, $row_s[nama_lengkap], $we);
	$e++;
}
  
} else {
  echo "ERROR: Could not execute $sql. " . print_r($pdo->errorInfo());
}

// close connection
unset($pdo);

// save file to disk
if ($excel->close() === true) {
  echo 'Spreadsheet successfully saved!';  
} else {
  echo 'ERROR: Could not save spreadsheet.';
}
?>