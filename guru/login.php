<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="shortcut icon" href="../images/simukel.png"/>

<title>SIMukel</title>
<link href="../css/templatemo_style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jquery.tools.min.js"></script>
<script type="text/javascript"> 
      $(document).ready(function(){
      	// expose form ketika form di klik atau kursor mouse berada di salah satu komponen form
	      $("form.expose").bind("click keydown", function() {
          $(this).expose({

			     // setting mask/penutup untuk background dengan CSS
			     maskId: 'mask',

			     // ketika form ter-expose, ganti warna background form
			     onLoad: function() {
				      this.getExposed().css({backgroundColor: ''});
			     },

			     // ketika form tidak ter-expose, kembalikan warna background ke warna semula
			     onClose: function() {
				      this.getExposed().css({backgroundColor: null});
			     },
			     api: true

		      }).load();
	      });
	    });
	  </script>	
</head>

<body class="log_ad">

<div id="templatemo_header">
	   <div class="logo">
        <h1><a href="#"><span>SI</span>MuKel<small>Sistem Informasi   Wali Murid &amp; Wali Kelas</small></a><a href="#"></a></h1>
 
       
      </div> 
</div> <!-- end of header --></br>
<div class="clear"></div>
<div class="login_admin">
	<form class="expose" action="../include/ceklogin.php" method="post">
	    <h3><img src="../images/icons/login_guru.png" /></h3>
        <div>
 		    <label>NIP:</label>
            <input type="text" name="nip" />
        </div>
        <div>
            <label>Password:</label>
            <input type="password" name="pass" />
        </div>
        <div class="bottom">
            <input type="submit" value="Login">
        	<div class="clear"></div>
        </div>
	</form>
</div>                
</body>
</html>